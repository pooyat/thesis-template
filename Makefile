# ===============================================================
# A Makefile for knitr and LaTeX 
# Pooya Taherkhani pt375611 @ ohio edu
# ===============================================================

MAINFILE := thesis

all: $(MAINFILE).pdf

$(MAINFILE).pdf: $(MAINFILE).tex
	latexmk -pdf $<

$(MAINFILE).tex: $(MAINFILE).Rnw chp*.Rnw chp*.tex chp*.R
	R -q -e "library(knitr);  knit('$<')"

view:
	evince $(MAINFILE).pdf

clean:
	latexmk -c
	rm -f *~ \#*

cleanall:
	make clean
	rm -f $(MAINFILE).bbl
